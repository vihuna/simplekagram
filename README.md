Simplekagram configuration file for TeX4ht
==========================================

Description
-----------

Basic **TeX4ht** configurations for HTML5 conversion using [Milligram][] 
framework, with SVG images or [KaTeX][] to display math formulas. It's designed
for small and medium-sized LaTeX articles, with the whole document placed in a
single HTML page.

- It uses **Flexbox** CSS for a responsive column layout.
- Table of contents located in left Flexbox column, avoiding HTML iframes.
- By default, "inline math" and "display math" unnumbered formulas are
  rendered using KaTeX; "Display math" numbered formulas are displayed as
  SVG images.


**Required LaTeX Packages**:  `etoolbox`, `environ` and `expl3`.

**Main Goal**: this configuration tries to adapt **TeX4ht** output to ensure
compatibility with KaTeX, so there is no need to modify the original
LaTeX document manually. To do this (with `katex-*` options), this configuration
transforms the necessary **LaTeX** math commands into other macros compatible
with the javascript library, maybe at the cost of losing some features.


Options
-------

### Custom options

- `katex-local`: It uses a local copy o KaTeX, inside the folder with name
  `katex`; this is the default option. 
- `katex-CDN`: It uses the KaTeX remote CDN.

In both, `katex-local` and `katex-CDN` options, "inline math" and "display
math" unnumbered formulas are rendered using KaTeX library. Numbered "display
math" formulas are represented as SVG images. 

- `svg-full`: Render all math formulas as SVG images.

- `TOC-sticky`: Sticky CSS position for the table of contents.

### TeX4ht options

- All pagination TeX4ht options are considered incompatible with this
  configuration file; unexpected results can be obtained.
- Command line options for other document formats (HTML4, DocBook, ...) instead
  of HTML5 will take no effect.
- Command line options for other image formats (PNG, GIF) instead of SVG will
  not work.


**Example of use:**

    make4ht -u -c simple -o output mydoc.tex "katex-CDN,TOC-sticky"


Known issues
------------

- Default vertical aligment and scaling of inline math images (when "svg-full"
  option is used) have poor quality. There is [no easy solution][] to this.

- The bibliography does not appear in the table of contents (as also happens
  when compiling to PDF)


Other considerations
--------------------

- It is highly recommended to run [Tidy][] after Make4ht, in order to obtain
  a strictly valid HTML5 document.
- Math rendering with KaTeX library avoids two problems with inline math
  images: vertical alignment, and resizing when the font size is changed.
  But you need to take into account [KaTeX limitations][]: it only supports a
  small set of math latex commands, and does not support cross-referencing at
  this moment.


[Milligram]: <https://milligram.io/>
[KaTeX]: <https://katex.org/>
[Tidy]: <https://www.html-tidy.org/>
[no easy solution]: <https://tex.stackexchange.com/questions/44486/pixel-perfect-vertical-alignment-of-image-rendered-tex-snippets/47271#47271>
[KaTeX limitations]: <https://katex.org/docs/support_table.html>
