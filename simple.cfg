%% Simplekagram, simple TeX4ht configuration and CSS styles for HTML5 output,
%% using Milligram CSS framework
%% v0.5, 2020/02/16
%% Víctor Huertas Navarro, http://reduccionalabsurdo.es
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\typeout{Simplekagram Simple-Milligram TeX4ht configuration}
\typeout{v0.5, 2020/02/16}
\typeout{Víctor Huertas, http://reduccionalabsurdo.es}


\RequirePackage{katex-latex-4ht}


%% Save the title of the document to use it with "TITLE+" configuration
\makeatletter
\let\mytitle\@title
\makeatother


%% Some elements from 4ht files with colons are needed, but colons have catcode
%% 12 in .cfg files. 

\catcode`: = 11
\newcommand{\usecharset}{\use:charset}
\catcode`: = 12


\AtBeginDocument{
%% A little trick to solve an annoying feature of 'tableofcontents' default
%% configuration: it does not handle toc title ¿...?. So next code empties the
%% macro that holds the TOC title, after making a backup.
\let\toctitbak\contentsname
\let\contentsname\undefined
}


%% TODO: Do not allow to divide the document in sections (if there is a way to
%% do it, or show warnings instead)


\Preamble{html5,xhtml,svg}


%% Options for math rendering
%% If two or more math rendering options are provided at the same time, this is
%% the order of preference: katex-full-CDN -> katex-full-local -> katex-CDN ->
%% -> svg-full -> katex-local 

\newbool{SvgMath}
\global\boolfalse{SvgMath}%
\newbool{KatexPartLocal}
\global\booltrue{KatexPartLocal}%
\newbool{KatexPartCDN}
\global\boolfalse{KatexPartCDN}%
\newbool{KatexFullLocal}
\global\boolfalse{KatexFullLocal}%
\newbool{KatexFullCDN}
\global\boolfalse{KatexFullCDN}%


\ifOption{svg-full}{
  \global\booltrue{SvgMath}
  \global\boolfalse{KatexPartLocal}
}{}

\ifOption{katex-CDN}{
  \global\booltrue{KatexPartCDN}
  \global\boolfalse{SvgMath}
  \global\boolfalse{KatexPartLocal}
}{}

\ifOption{katex-full-local}{
  \global\booltrue{KatexFullLocal}
  \global\boolfalse{SvgMath}
  \global\boolfalse{KatexPartLocal}
  \global\boolfalse{KatexPartCDN}
}{}

\ifOption{katex-full-CDN}{
  \global\booltrue{KatexFullCDN}
  \global\boolfalse{SvgMath}
  \global\boolfalse{KatexPartLocal}
  \global\boolfalse{KatexPartCDN}
  \global\boolfalse{KatexFullLocal}
}{}


%% HTML document configurations
%% \Configure{PROLOG}{DOCTYPE}
%% \Configure{DOCTYPE}{\HCode{<!DOCTYPE html>\Hnewline}}
%% \Configure{HTML}{\HCode{<html>\Hnewline}}{\HCode{</html>}}


%% Clear and redesign the HTML header so jobname.css can be the last stylesheet
%% to load.

\Configure{@HEAD}{}
\Configure{TITLE+}{\mytitle}
\Configure{@HEAD}{\HCode{<meta \usecharset />\Hnewline}}
\Configure{@HEAD}{%
  \HCode{%
    <meta name="viewport" content="width=device-width,initial-scale=1" />%
    \Hnewline}%
}

%% Link milligram and normalize stylesheets

\Configure{@HEAD}{%
  \HCode{%
    <link rel="stylesheet" type="text/css" href="milligram/normalize.css">%
    \Hnewline%
  }%
}

\Configure{@HEAD}{%
  \HCode{%
    <link rel="stylesheet" type="text/css" href="milligram/milligram.min.css">%
    \Hnewline%
  }%
}

\Configure{@HEAD}{%
  \HCode{%
    <link rel="stylesheet" type="text/css" href="milligram/blue.css">%
    \Hnewline%
  }%
}  

%% Add links to load local copy of KaTeX 


\ifboolexpr{bool {KatexPartLocal} or bool {KatexFullLocal}}{%
  \Configure{@HEAD}{%
    \HCode{%
      <link rel="stylesheet" href="katex-only-woff/katex.min.css">\Hnewline%
    }%
    \HCode{%
      <script defer src="katex-only-woff/katex.min.js"></script>\Hnewline%
    }%
    \HCode{%
      <script defer src="katex-only-woff/contrib/auto-render.min.js" onload="%
      renderMathInElement(document.body, {%
        delimiters: [% Disable dollars as delimiters for math content in KaTeX
        % They are no longer necessary because they have been converted
        % to a safer delimiters for HTML, in their configurations.
        % Add 'math' and 'displaymath' delimiters for math content
        {left: \detokenize{'\\(', right: '\\)'}, display: false},%
        {left: \detokenize{'\\[', right: '\\]'}, display: true},%
        {left: \detokenize{'\\begin{math}', right: '\\end{math}'}, display: false},%
        {left: \detokenize{'\\begin{displaymath}', right: '\\end{displaymath}'}, display: true},%
        ]%
      });"></script>\Hnewline%
    }%
  }%
}{}

%% Add links to load KaTeX CDN at the header

\ifboolexpr{bool {KatexPartCDN} or bool {KatexFullCDN}}{%
  \Configure{@HEAD}{%
    \HCode{%
      <link rel="stylesheet" %
      href="https://cdn.jsdelivr.net/npm/katex@0.10.2/dist/katex.min.css" %
      integrity="sha384-yFRtMMDnQtDRO8rLpMIKrtPCD5jdktao2TV19YiZYWMDkUR5GQZR/NOVTdquEx1j" %
      crossorigin="anonymous">\Hnewline%
    }%
    \HCode{%
      <script defer %
      src="https://cdn.jsdelivr.net/npm/katex@0.10.2/dist/katex.min.js" %
      integrity="sha384-9Nhn55MVVN0/4OFx7EE5kpFBPsEMZxKTCnA+4fqDmg12eCTqGi6+BB2LjY8brQxJ" %
      crossorigin="anonymous"></script>\Hnewline%
    }%
    \HCode{%
      <script defer %
      src="https://cdn.jsdelivr.net/npm/katex@0.10.2/dist/contrib/auto-render.min.js" %
      integrity="sha384-kWPLUVMOks5AQFrykwIup5lo0m3iMkkHrD0uJ4H5cjeGihAutqP0yW0J6dpFiVkI" %
      crossorigin="anonymous" onload="renderMathInElement(document.body, {%
        delimiters: [% Disable dollars as delimiters for math content in KaTeX
        % Add 'math' and 'displaymath' delimiters
        {left: \detokenize{'\\(', right: '\\)'}, display: false},%
        {left: \detokenize{'\\[', right: '\\]'}, display: true},%
        {left: \detokenize{'\\begin{math}', right: '\\end{math}'}, display: false},%
        {left: \detokenize{'\\begin{displaymath}', right: '\\end{displaymath}'}, display: true},%
        ]%
      });"></script>\Hnewline%
    }%
  }%
}{}

%% Load the CSS generated by TeX4ht

\Configure{@HEAD}{%
  \HCode{<link rel="stylesheet" type="text/css" href="\jobname.css">\Hnewline}%
}

%% Load custom CSS for the HTML skeleton with Milligram

\Configure{@HEAD}{%
  \HCode{<link rel="stylesheet" type="text/css" href="simple.css">\Hnewline}%
}


%% Body configuration, adding the skeleton of the HTML document 

\Configure{BODY}{%
  \HCode{%
    \Hnewline<body>\Hnewline<main class="container"><div class="row">\Hnewline%
    <div id="aux-content-1" class="column">\Hnewline%
  }%
}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{%
    </div>\Hnewline</div></main>\Hnewline</body>%
  }%
}


%% Title configuration

\Configure{maketitle}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{%
    </div>\Hnewline<header id="title" class="column">%
  }%
}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{%
    </header>\Hnewline<div id="aux-content-2" class="column">%
  }%
}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{<h1 class="titleHead">}\NoFonts%
}{%
  \ifvmode\IgnorePar\fi\EndP\EndNoFonts\HCode{</h1>}%
}


%% Configurations for the table of contents

\Configure{tableofcontents}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{%
    </div>\Hnewline<nav id="nav-toc" class="column">\Hnewline<h2>%
  }%
  \toctitbak\HCode{</h2>\Hnewline<ul>}%
}
{}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{%
    </ul>\Hnewline</nav>\Hnewline<div id="aux-content-3" class="column">%
  }%
}
{}{}

% \ConfigureToc{section}{ }{\NoFonts\HCode{<p>}}{\HCode{</p>}\EndNoFonts}{ }

\ConfigureToc{section}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{<li class="nav-section">}%
}
{\ }{}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{</li>\Hnewline}%
}

% \ConfigureToc{subsection} {\empty}{\ }{}{\newline}

\ConfigureToc{subsection}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{<li class="nav-subsection">}%
}
{\ }{}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{</li>\Hnewline}%
}

%% By default, do not show subsubsections in the TOC

\ConfigureToc{subsubsection}{}{}{}{}


%% Configurations for the section headers; just to add HTML5 section tags

\Configure{section}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{<section>\Hnewline}%
}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{</section>\Hnewline}%
}
{}{}

\Configure{likesection}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{<section>\Hnewline}%
}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{</section>\Hnewline}%
}
{}{}

\Configure{subsection}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{<section>\Hnewline}%
}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{</section>\Hnewline}%
}
{}{}

\Configure{likesubsection}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{<section>\Hnewline}%
}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{</section>\Hnewline}%
}
{}{}

\Configure{subsubsection}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{<section>\Hnewline}%
}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{</section>\Hnewline}%
}
{}{}

\Configure{likesubsubsection}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{<section>\Hnewline}%
}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{</section>\Hnewline}%
}
{}{}


%% Text style configurations

\Configure{textbf}{\HCode{<strong>}\NoFonts}{\EndNoFonts\HCode{</strong>}}


%% Math environments configuration

\ifbool{SvgMath}{

  % Render the whole inline math formula as a single image

  \Configure{$\iffalse $ trick to avoid an error in editor syntax hightlighting\fi
  }{\PicMath}{\EndPicMath}{}

  % Render the whole display-formulas as a single image
    
  \ConfigureEnv{equation*}{\PicDisplay}{\EndPicDisplay}{}{}
  \ConfigureEnv{multline*}{\PicDisplay}{\EndPicDisplay}{}{}
  \ConfigureEnv{align*}{\PicDisplay}{\EndPicDisplay}{}{}
  
}{}


\ifboolexpr{bool {KatexPartLocal} or bool {KatexPartCDN} or bool {SvgMath}}{
  \AtBeginDocument{

    %% Equation numbering in gather environment is not aligned vertically in
    %% the column with the corresponding row.
    %% This configuration displays the whole content of gather environment as
    %% an image, to avoid this issue.
    %% Similar issues with 'multline' and 'align'

    \ConfigureEnv{gather}{\PicDisplay}{\EndPicDisplay}{}{}
    \ConfigureEnv{multline}{\PicDisplay}{\EndPicDisplay}{}{}
    \ConfigureEnv{align}{\PicDisplay}{\EndPicDisplay}{}{}
    \ConfigureEnv{equation}{\PicDisplay}{\EndPicDisplay}{}{}


    %% Default configuration of PicDisplay, with '<center>' tag is incompatible
    %% with HTML5. This configuration uses CSS to center the formula.

    \Configure{PicDisplay}{%
      \ifvmode\IgnorePar\fi\EndP%
      \HCode{<div class="math-display">}%
    }
    {\HCode{</div>}}
    {}{}

  }
}{}


%% Apply configurations to keep latex syntax when katex option is used

\ifboolexpr{bool {KatexPartLocal} or bool {KatexPartCDN} %
  or bool {KatexFullLocal} or bool {KatexFullCDN}%
}{
  \AtBeginDocument{
    
    \Configure{$\iffalse $ avoid an error in editor syntax hightlighting\fi
    }{}{}{\expandafter\AltMathOne}

    \Configure{()}{\AltlMath}{}
    
    \VerbMath{math}
    
    \Configure{$$\iffalse $$ avoid an error in editor syntax hightlighting\fi
    }{%
      \ifvmode\IgnorePar\fi\EndP\HCode{<div class="katex-display-block">}%
    }{%
      \ifvmode\IgnorePar\fi\EndP\HCode{</div>}%
    }{%
      \expandafter\AltlDisplayOne%
    }
    
    \Configure{[]}{%
      \ifvmode\IgnorePar\fi\EndP\HCode{<div class="katex-display-block">}%
      \AltlDisplay%
    }{%
      \ifvmode\IgnorePar\fi\EndP\HCode{</div>}%
    }
    
    \ConfigureEnv{displaymath}{%
      \ifvmode\IgnorePar\fi\EndP\HCode{<div class="katex-display-block">}%
    }{%
      \ifvmode\IgnorePar\fi\EndP\HCode{</div>}%
    }{}{}

    \VerbMath{displaymath}

    %% KaTeX still does not support other stand-alone math-mode environments
    %% like 'equation', 'multline' or 'gather'.
    %% The environments 'aligned' and 'gathered' can be used (inside some of
    %% supported math modes) for multi-line math equations.
    %% Another features not yet supported by KaTeX are the automatic numbering
    %% of formulas and cross-referencing.

    %% Math environments converted to gathered

    \ConfigureEnv{gather*}{%
      \ifvmode\IgnorePar\fi\EndP\HCode{<div class="katex-display-block">}%
    }{%
      \ifvmode\IgnorePar\fi\EndP\HCode{</div>}%
    }{}{}
    
    \VerbEnv{gather*}{gathered}
    
    \ConfigureEnv{multline*}{%
      \ifvmode\IgnorePar\fi\EndP\HCode{<div class="katex-display-block">}%
    }{%
      \ifvmode\IgnorePar\fi\EndP\HCode{</div>}%
    }{}{}
    
    \VerbEnv{multline*}{gathered}

    %% Math environments converted to aligned

    \ConfigureEnv{align*}{%
      \ifvmode\IgnorePar\fi\EndP\HCode{<div class="katex-display-block">}%
    }{%
      \ifvmode\IgnorePar\fi\EndP\HCode{</div>}%
    }{}{}
    
    \VerbEnv{align*}{aligned}
    
    %% Math environmentes converted to displaymath
    
    \ConfigureEnv{equation*}{%
      \ifvmode\IgnorePar\fi\EndP\HCode{<div class="katex-display-block">}%
    }{%
      \ifvmode\IgnorePar\fi\EndP\HCode{</div>}%
    }{}{}

    \VerbEnvSA{equation*}{displaymath}

  }% AtBeginDocument
}{}% ifboolexpr


% \ifboolexpr{bool {KatexFullLocal} or bool {KatexFullCDN}}{%
% \AtBeginDocument{%
%
%% Numbered math environments converted to gathered


%% Numbered math environments converted to aligned


%% Numbered math environmentes converted to displaymath
% }
% }{}


%%  HTML5 figures

\ConfigureEnv{figure}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{<figure>}\HtmlParOff%
  \ShowPar\Configure{float}{}{}{}%
}{%
  \ifvmode\IgnorePar\fi\EndP\HCode{</figure>}\HtmlParOn\ShowPar%
}
{}{}

%% HTML5 figure captions

\Configure{caption}{%
  \HCode{<figcaption><span class="id">}%
}
{: }
{\HCode{</span><span \Hnewline class="content">}}
{\HCode{</span></figcaption>}}


%% Always remove 'hlines' inside table; a default style for all table borders
%% is added using CSS 

\Configure{HBorder}{}{}{}{}{}{}{}{}{}{}


%% Custom css styles for HTML output

\Css{.container {padding-top: 3em;}}

%% Navigation menu

\Css{\#nav-toc ul {list-style: none;}}
\Css{\#nav-toc li.nav-subsection {margin-left: 1em;}}

%% Title, headers

\Css{.titleHead {text-align: center}}
\Css{.date {margin-top: -1em}}
\Css{h1, h2, h3, h4, h5, h6 {font-family: sans;	font-weight: bolder;%
    padding-top: 1.5em;}}
\Css{\#nav-toc h2 {padding-top: 0;}}

%% Figures, floatings, images

\Css{%
  figure {%
    margin: 1.5em 0;%
    padding: 1.5em 0 1em 0;%
    border-bottom: 0.1rem solid \#e1e1e1; border-top: 0.1rem solid \#e1e1e1;%
  }%
}
\Css{%
  figure img {%
    display: block; margin-left: auto; margin-right: auto; max-width: 90\%;%
  }%
}
\Css{%
  figure figcaption {%
    display: block;%
    margin-top: 0.5em;%
    font-size: 1.6rem;%
    text-align: center;%
    max-width: 75\%;%
    margin-left: auto;%
    margin-right: auto;%
  }%
}
\Css{figcaption span.id {font-weight: bold}}

%% Verbatim environment

\Css{%
  div .verbatim {%
    font-size: 14px;%
    font-family: monospace;%
    border: 1px solid \#ccc;%
    border-radius: 4px;
    padding: 1em;
    overflow-x: auto;
    background-color: \#f5f5f5;%
  }
}

% CSS styles for tables (tabular)

\Css{div .tabular {overflow-x: auto;}}

\Css{div .tabular table {border: 1px solid black;}}

\Css{div .tabular table tr:first-child td {background-color: \#f5f5f5;}}

\Css{%
  div .tabular table tr:last-child td {border-bottom: none;}
}


%% Math images

\Css{%
  div.math-display {%
    padding-top: 1em; padding-bottom: 1.5em; overflow: auto;%
    text-align: center;%
  }%
}
\Css{%
  div.math-display img {%
     max-width: none;%
  }%
}

%% KaTeX math rendering

\Css{.katex {font-size: 1.1em;}}
\Css{.katex-display-block {overflow-x: auto;}}

%% TeX4ht option to change the TOC menu CSS style to 'sticky'

\ifOption{TOC-sticky}%
{%
\Css{nav {position: sticky; top: 1em; overflow-y: auto;%
max-height: calc(100vh - 2rem);}}%
}{}


%% Track down the necessary files for the output

%% Custom stylesheet

\special{t4ht+@File: simple.css}

%% Fonts

\special{t4ht+@File: fonts/STIX2Math.woff}
\special{t4ht+@File: fonts/STIX2Text-Bold.woff}
\special{t4ht+@File: fonts/STIX2Text-BoldItalic.woff}
\special{t4ht+@File: fonts/STIX2Text-Italic.woff}
\special{t4ht+@File: fonts/STIX2Text-Regular.woff}

%% Milligram stylesheets

\special{t4ht+@File: milligram/aqua.css}
\special{t4ht+@File: milligram/blue.css}
\special{t4ht+@File: milligram/gray.css}
\special{t4ht+@File: milligram/milligram.css.map}
\special{t4ht+@File: milligram/milligram.min.css}
\special{t4ht+@File: milligram/milligram.min.css.map}
\special{t4ht+@File: milligram/navy.css}
\special{t4ht+@File: milligram/normalize.css}
\special{t4ht+@File: milligram/silver.css}
\special{t4ht+@File: milligram/teal.css}

%% KaTeX files

\ifOption{katex-local}{%
\special{t4ht+@File: katex-only-woff/katex.min.css}%
\special{t4ht+@File: katex-only-woff/katex.min.js}%
\special{t4ht+@File: katex-only-woff/contrib/auto-render.min.js}%
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_AMS-Regular.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Caligraphic-Bold.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Caligraphic-Regular.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Fraktur-Bold.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Fraktur-Regular.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Main-Bold.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Main-BoldItalic.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Main-Italic.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Main-Regular.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Math-BoldItalic.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Math-Italic.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_SansSerif-Bold.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_SansSerif-Italic.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_SansSerif-Regular.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Script-Regular.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Size1-Regular.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Size2-Regular.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Size3-Regular.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Size4-Regular.woff}
\special{t4ht+@File: katex-only-woff/fonts/KaTeX_Typewriter-Regular.woff}
}{}


\begin{document}


\EndPreamble

